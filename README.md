MessageChanger is a plugin allowing you to change the default messages of Spout / Vanilla / and other games (hopefully) based on permissions.

It also allows changing of the default deathmessages with random ones based on the deathcause.

It is easy to extend, so if you have deathcauses which are not supported (because they are added by a plugin or game) let the developer of that plugin / game and me know so that we can update this plugin.

Pull Requests are always welcome and earn you a place in the hall of fame... �hh... pom.xml :-)

If you want to use MessageChanger go here: https://github.com/dredhorse/MessageChanger/wiki

Doxygen Documentation can be found here http://dredhorse.github.com/MessageChanger/doxygen/index.html

further documentation incl. normal javadocs can be found here: http://dredhorse.github.com/MessageChanger

Maven Repo is at: https://don-redhorse-repo.googlecode.com/svn/trunk/

If you want to help feel free to do so.

Feel free to use and comment.



/*
 * This file is part of MessageChanger.
 *
 * MessageChanger is licensed under the SpoutDev License Version 1.
 *
 * MessageChanger is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the SpoutDev License Version 1.
 *
 * Main is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License,
 * the MIT license and the SpoutDev License Version 1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.spout.org/SpoutDevLicenseV1.txt> for the full license,
 * including the MIT license.
 */

